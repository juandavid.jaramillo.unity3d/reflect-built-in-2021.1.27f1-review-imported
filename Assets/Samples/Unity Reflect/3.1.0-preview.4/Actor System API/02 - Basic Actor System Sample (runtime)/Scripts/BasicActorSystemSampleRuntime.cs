using Unity.Reflect.ActorFramework;
using Unity.Reflect.Model;
using UnityEngine;
using UnityEngine.Reflect;

namespace Unity.Reflect.Actors.Samples
{
    public class BasicActorSystemSampleRuntime : MonoBehaviour
    {
        [SerializeField]
        SampleMaterialConverter m_MaterialConverter;

        public Transform root;

        void Start()
        {
            // Create the actor setups and connections
            var asset = CreateBasicActorSystemAsset();

            // Assign root transform to GameObjectBuilderActor
            var gameObjectBuilderSetup = asset.GetActorSetup<GameObjectBuilderActor>();
            gameObjectBuilderSetup.GetActorSettings<GameObjectBuilderActor.Settings>().Root = root;

            // Instantiate and start the system
            // Create the scene component
            var reflectBootstrapper = gameObject.AddComponent<ReflectActorSystem>();

            // Assign the ActorSystem asset to the RuntimeReflectBootstrapper
            reflectBootstrapper.Asset = asset;
            
            // Initialize and start the system
            reflectBootstrapper.Instantiate();
            reflectBootstrapper.StartActorSystem();
            
            // Call the RpcInput that will trigger the streaming
            reflectBootstrapper.ForwardRpc<ManifestActor, GetManifests, NullData>(new GetManifests(new GetManifestOptions()));
        }

        ActorSystemSetup CreateBasicActorSystemAsset()
        {
            // Create the system asset
            var asset = ActorUtils.CreateActorSystemSetup();

            // Create the actors
            var manifestSetup = asset.CreateActorSetup<ManifestActor>();                       // load data from the manifest
            var instanceStreamingSetup = asset.CreateActorSetup<InstanceStreamingActor>();     // stream instance entries in batches

            var gameObjectCreatorSetup = asset.CreateActorSetup<GameObjectCreatorActor>();     // gather SyncMesh, SyncMaterial and SyncTexture dependencies
            var gameObjectConverterSetup = asset.CreateActorSetup<GameObjectConverterActor>(); // gather Mesh, Material and Texture asset dependencies
            var gameObjectBuilderSetup = asset.CreateActorSetup<GameObjectBuilderActor>();     // instantiate GameObject
            
            var unityTextureSetup = asset.CreateActorSetup<UnityTextureActor>();               // manage Texture assets
            var textureConverterSetup = asset.CreateActorSetup<TextureConverterActor>();       // convert SyncTexture to Texture
            
            var unityMeshSetup = asset.CreateActorSetup<UnityMeshActor>();                     // manage Mesh assets
            var meshConverterSetup = asset.CreateActorSetup<MeshConverterActor>();             // convert SyncMesh to Mesh
            
            var unityMaterialSetup = asset.CreateActorSetup<UnityMaterialActor>();             // manage Material assets
            var materialConverterSetup = asset.CreateActorSetup<MaterialConverterActor>();     // convert SyncMaterial to Material
            
            var modelResourceSetup = asset.CreateActorSetup<ModelResourceActor>();             // manage SyncModel
            var dataProviderSetup = asset.CreateActorSetup<SampleDataProviderActor>();         // provide access to resource files

            var jobSetup = asset.CreateActorSetup<JobActor>();                                 // run jobs on the main thread

            var triggerActor = asset.CreateActorSetup<SampleStreamInstanceTriggerActor>();

            asset.ConnectRpc<GetManifests>(manifestSetup, dataProviderSetup);
            asset.ConnectRpc<CreateGameObject>(instanceStreamingSetup, gameObjectCreatorSetup);

            asset.ConnectRpc<AcquireResource>(gameObjectCreatorSetup, modelResourceSetup);
            asset.ConnectRpc<AcquireEntryDataFromModelData>(gameObjectCreatorSetup, manifestSetup);
            asset.ConnectRpc<AcquireDynamicEntry>(gameObjectCreatorSetup, manifestSetup);
            asset.ConnectRpc<ConvertToGameObject>(gameObjectCreatorSetup, gameObjectConverterSetup);
            
            asset.ConnectRpc<AcquireUnityMaterial>(gameObjectConverterSetup, unityMaterialSetup);
            asset.ConnectNet<ReleaseUnityMaterial>(gameObjectConverterSetup, unityMaterialSetup);
            
            asset.ConnectRpc<AcquireUnityMesh>(gameObjectConverterSetup, unityMeshSetup);
            asset.ConnectNet<ReleaseUnityMesh>(gameObjectConverterSetup, unityMeshSetup);
            
            asset.ConnectRpc<AcquireUnityTexture>(materialConverterSetup, unityTextureSetup);
            asset.ConnectNet<ReleaseUnityTexture>(materialConverterSetup, unityTextureSetup);
            
            asset.ConnectRpc<GetProjectWideDefaultMaterial>(gameObjectConverterSetup, materialConverterSetup);
            asset.ConnectRpc<AcquireEntryDataFromModelData>(gameObjectConverterSetup, manifestSetup);
            
            asset.ConnectRpc<BuildGameObject>(gameObjectConverterSetup, gameObjectBuilderSetup);

            asset.ConnectRpc<ConvertResource<SyncMesh>>(unityMeshSetup, meshConverterSetup);
            asset.ConnectRpc<ConvertResource<SyncTexture>>(unityTextureSetup, textureConverterSetup);
            asset.ConnectRpc<ConvertSyncMaterial>(unityMaterialSetup, materialConverterSetup);

            asset.ConnectRpc<AcquireResource>(unityTextureSetup, modelResourceSetup);
            asset.ConnectRpc<AcquireResource>(unityMaterialSetup, modelResourceSetup);
            asset.ConnectRpc<AcquireResource>(unityMeshSetup, modelResourceSetup);
            
            asset.ConnectRpc<GetSyncModel>(modelResourceSetup, dataProviderSetup);
            
            asset.ConnectRpc<AcquireEntryDataFromModelData>(unityMaterialSetup, manifestSetup);

            asset.ConnectRpc<DelegateJob>(meshConverterSetup, jobSetup);
            asset.ConnectRpc<DelegateJob>(unityMaterialSetup, jobSetup);
            asset.ConnectRpc<DelegateJob>(unityTextureSetup, jobSetup);
            asset.ConnectRpc<DelegateJob>(unityMeshSetup, jobSetup);

            asset.ConnectNet<UpdateStreaming>(triggerActor, instanceStreamingSetup);
            asset.ConnectNet<DynamicEntryChanged>(manifestSetup, triggerActor);

            var converterSettings = materialConverterSetup.GetActorSettings<MaterialConverterActor.Settings>();
            converterSettings.Converters = new ActorFramework.IReflectMaterialConverter[] { m_MaterialConverter };

            var builderSettings = gameObjectBuilderSetup.GetActorSettings<GameObjectBuilderActor.Settings>();
            builderSettings.LightImporter = new SyncLightImporter();

            return asset;
        }
    }
}
